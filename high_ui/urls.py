from django.urls import path

from .views import PlayView
from .views import HomeView

app_name = "high_ui"

urlpatterns = [
    path(r"", HomeView.as_view(), name="splash"),
    path(r"play/", PlayView.as_view(), name="play"),
]
